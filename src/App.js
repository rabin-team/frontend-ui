//import logo from './google.jpg';
import './App.css';
import Greek from './components/Greek';
import Test from './components/Test'
import Footer from './components/Footer'; 
function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hello World...Welcome my Page
        </p>
        <a
          className="App-link"
          href="https://google.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Open Google Home Page.
        </a>
      </header> */}
      <Greek />
      <Test />
      <Footer />
      <Test2 />
    </div>
  );
}

export default App;
